package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    @FXML TextField tfUserName = new TextField();
    @FXML PasswordField pfPasswordField = new PasswordField();
    @FXML Button btnLogin = new Button();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        setupLoginButtonListener();


    }

    public void setupLoginButtonListener(){
        btnLogin.setOnAction(event -> {
//            AnchorPane dashboard = null;
//            try {
//                dashboard = FXMLLoader.load(getClass().getResource("dashboard.fxml"));
//            } catch (IOException e) {
//                System.out.println("dashboard is still null. THERE IS SOME ERROR");
//                e.printStackTrace();
//            }

            String user = tfUserName.getText();
            System.out.println("username is " + user);
            String password = pfPasswordField.getText();
            System.out.println("password is " + password);

            if(login(user, password) != null){//if login successful
                System.out.println("login successful");
                //set currentuser

                //load the dashboard screen
                Stage newStage = (Stage) btnLogin.getScene().getWindow();
                //newStage.setScene(new Scene(new AnchorPane(), 1000, 700));
                AnchorPane ap = new AnchorPane();
                ap.getChildren().add(new Label("this is new anchorpane"));
                newStage.setScene(new Scene(ap, 1000, 700));
                newStage.show();
            }
            else {
                System.out.println("login NOT successful");
            }
        });
    }

    public JSONObject login(String username, String password){
        //still need to implement login logic
        JSONObject justANotNullObject = new JSONObject();
        return justANotNullObject;
    }
}
